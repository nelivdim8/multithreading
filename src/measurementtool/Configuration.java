package measurementtool;

public class Configuration {
    private int producersCount;
    private int consumersCount;

    public Configuration(int producers, int consumers) {
        super();
        this.producersCount = producers;
        this.consumersCount = consumers;
    }

    public int getProducersCount() {
        return producersCount;
    }

    public int getConsumersCount() {
        return consumersCount;
    }

    @Override
    public String toString() {
        return "Producers:" + " - " + producersCount + ", Consumers: " + consumersCount + "\n";
    }

}
