package measurementtool;

import java.util.concurrent.ExecutionException;

public class MeasurementToolDemo {
	public static void main(String[] args) {
		MeasurementTool tool1 = new MeasurementTool(200000, 100000, 3000000);
		try {
			tool1.executeTask(4, 6);
			tool1.executeTask(4, 4);
			tool1.executeTask(6, 4);
			tool1.executeTask(1, 1);
			tool1.executeTask(8, 8);
			System.out.println("Time 1 " + tool1.getStatistics());

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
