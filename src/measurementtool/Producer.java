package measurementtool;

import java.util.concurrent.BlockingQueue;

public class Producer implements Runnable {
    private BlockingQueue<Integer> queue;
    private int elementsToProduce;

    public Producer(BlockingQueue<Integer> queue, int elementsToProduce) {
        super();
        this.queue = queue;
        this.elementsToProduce = elementsToProduce;
    }

    @Override
    public void run() {
        for (int i = 0; i < elementsToProduce; i++) {

            queue.offer(i);
            // System.out.println("Thread " + Thread.currentThread().getName() + "- add " +
            // i);

        }

    }

}
