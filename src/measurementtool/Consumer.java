package measurementtool;

import java.util.concurrent.BlockingQueue;

public class Consumer implements Runnable {
    private BlockingQueue<Integer> queue;
    private int elementsToRemove;

    public Consumer(BlockingQueue<Integer> queue, int elementsToRemove) {
        super();
        this.queue = queue;
        this.elementsToRemove = elementsToRemove;
    }

    @Override
    public void run() {
        for (int i = 0; i < elementsToRemove; i++) {

            queue.poll();
            // System.out.println("Thread " + Thread.currentThread().getName() + "- remove "
            // + i);

        }
    }
}
