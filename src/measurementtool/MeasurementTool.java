package measurementtool;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutionException;

public class MeasurementTool {
	private int elementsToAdd;
	private int elementsToRemove;
	private int size;
	private BlockingQueue<Integer> elements;
	private Map<Long, List<Configuration>> timeAndConfiguration;

	public MeasurementTool(int elementsToAdd, int elementsToRemove, int size) {
		super();
		this.elementsToAdd = elementsToAdd;
		this.elementsToRemove = elementsToRemove;
		this.elements = new ArrayBlockingQueue<>(size);
		this.size = size;
		this.timeAndConfiguration = new TreeMap<>();

	}

	public long executeTask(int producers, int consumers) throws InterruptedException, ExecutionException {

		List<Thread> workers = new ArrayList<>();
		long timeStarted = System.currentTimeMillis();

		for (int i = 0; i < producers; i++) {
			Thread worker = new Thread(
					new measurementtool.Producer(elements, getElementsCountPerThread(elementsToAdd, producers)));
			worker.start();
		}
		for (int i = 0; i < consumers; i++) {
			Thread worker = new Thread(
					new measurementtool.Consumer(elements, getElementsCountPerThread(elementsToRemove, consumers)));
			workers.add(worker);
			worker.start();
		}
		joinThreads(workers);
		long timeTaken = System.currentTimeMillis() - timeStarted;
		addToStatistics(timeTaken, producers, consumers);
		return timeTaken;

	}

	private void addToStatistics(long timeTaken, int producers, int consumers) {
		if (timeAndConfiguration.get(timeTaken) != null) {
			timeAndConfiguration.get(timeTaken).add(new Configuration(producers, consumers));
			
		} else {
			List<Configuration> elements = new ArrayList<>();
			elements.add(new Configuration(producers, consumers));
			timeAndConfiguration.put(timeTaken, elements);

		}

	}

	private void joinThreads(List<Thread> workers) throws InterruptedException {
		for (Thread thread : workers) {
			thread.join();
		}

	}

	private int getElementsCountPerThread(int elementsCount, int workersCount) {
		int diff = elementsCount - workersCount;
		if (diff > 0) {
			elementsCount = diff;
			return workersCount;
		}

		return elementsCount;
	}

	private String timeAndConfigurationToString() {

		StringBuilder sb = new StringBuilder();
		for (Map.Entry<Long, List<Configuration>> entry : timeAndConfiguration.entrySet()) {
			sb.append("Time: " + entry.getKey() + "\n");
			for (Configuration configuration: entry.getValue()) {
				sb.append(configuration.toString());

			}
		}
		return sb.toString();
	}

	public String getStatistics() {
		return "Data structure capacity: " + size + "\n" + "Elements to add: " + elementsToAdd + "\n"
				+ "Elements to remove: " + elementsToRemove + "\n" + timeAndConfigurationToString() + "\n";
	}

}
