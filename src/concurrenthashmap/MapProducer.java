package concurrenthashmap;

import java.util.Map;

public class MapProducer implements Runnable {
    private final Map<String, Integer> elements;
    private final int elementsToAdd;
    private final int from;

    public MapProducer(Map<String, Integer> elements, int from, int elementsToAdd) {
        super();
        this.elements = elements;
        this.elementsToAdd = elementsToAdd;
        this.from = from;
    }

    @Override
    public void run() {
        for (int i = from; i < elementsToAdd; i++) {
            String element = "index" + i;
            elements.put(element, i);
        }

    }
}
