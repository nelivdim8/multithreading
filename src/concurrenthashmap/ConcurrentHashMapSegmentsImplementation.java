package concurrenthashmap;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;

public class ConcurrentHashMapSegmentsImplementation<K, V> implements Map<K, V> {

    private final HashMap<K, V>[] segments;
    private final static int INITIAL_SIZE = 8;
    private volatile int size;
    private final ReentrantReadWriteLock lock;
    private final ReadLock readLock;

    public ConcurrentHashMapSegmentsImplementation() {
        this.segments = (HashMap<K, V>[]) new HashMap[INITIAL_SIZE];
        initializeEntries();
        this.size = 0;
        this.lock = new ReentrantReadWriteLock();
        this.readLock = lock.readLock();

    }

    private void initializeEntries() {
        for (int i = 0; i < segments.length; i++) {
            segments[i] = new HashMap<>();
        }
    }

    private int getKeyHashCode(String key) {
        int hash = 1;
        int base = 29;
        for (int i = 0; i < key.length(); i++) {
            hash = (hash * base + key.charAt(i)) % segments.length;
        }
        return hash;
    }

    public V put(K key, V value) {
        int index = getKeyHashCode((String) key);
        synchronized (segments[index]) {
            size++;
            return segments[index].put(key, value);

        }
    }

    public boolean containsKey(Object key) {
        int index = getKeyHashCode((String) key);
        synchronized (segments[index]) {
            return segments[index].containsKey(key);
        }

    }

    public V get(Object key) {
        int index = getKeyHashCode((String) key);
        synchronized (segments[index]) {
            if (segments[index].containsKey(key)) {
                return segments[index].get(key);
            }
            return null;
        }
    }

    public V remove(Object key) {
        int index = getKeyHashCode((String) key);
        synchronized (segments[index]) {
            return segments[index].remove(key);
        }

    }

    public boolean isEmpty() {
        return size == 0;

    }

    public int size() {
        return size;
    }

    @Override
    public boolean containsValue(Object key) {
        readLock.lock();
        for (HashMap<K, V> hashMap : segments) {
            if (hashMap.containsValue(key)) {
                readLock.unlock();
                return true;
            }

        }
        readLock.unlock();
        return false;
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        readLock.lock();
        Set<Entry<K, V>> entries = new HashSet<>();
        for (HashMap<K, V> hashMap : segments) {
            entries.addAll(hashMap.entrySet());

        }

        readLock.unlock();
        return entries;

    }

    @Override
    public Set<K> keySet() {
        readLock.lock();
        Set<K> keys = new HashSet<>();
        for (HashMap<K, V> hashMap : segments) {
            keys.addAll(hashMap.keySet());

        }
        readLock.unlock();
        return keys;

    }

    @Override
    public Collection<V> values() {
        readLock.lock();
        Collection<V> values = new ArrayList<>();
        for (HashMap<K, V> hashMap : segments) {
            values.addAll(hashMap.values());

        }
        readLock.unlock();
        return values;
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        readLock.lock();
        for (Map.Entry<? extends K, ? extends V> e : m.entrySet()) {
            K key = e.getKey();
            V value = e.getValue();
            put(key, value);
        }

        readLock.unlock();

    }

    @Override
    public void clear() {
        readLock.lock();
        for (HashMap<K, V> hashMap : segments) {
            hashMap.clear();

        }
        readLock.unlock();

    }

}
