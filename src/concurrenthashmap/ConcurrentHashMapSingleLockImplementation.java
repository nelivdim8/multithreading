package concurrenthashmap;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;

public class ConcurrentHashMapSingleLockImplementation<K, V> implements Map<K, V> {
    private final HashMap<K, V> entries;
    private final ReentrantReadWriteLock lock;
    private final WriteLock writeLock;
    private final ReadLock readLock;
    private volatile int size;

    public ConcurrentHashMapSingleLockImplementation() {
        this.entries = new HashMap<>();
        this.lock = new ReentrantReadWriteLock();
        this.writeLock = lock.writeLock();
        this.readLock = lock.readLock();

    }

    @Override
    public V put(K key, V value) {
        writeLock.lock();
        V insertedValue = entries.put(key, value);
        writeLock.unlock();
        return insertedValue;
    }

    @Override
    public boolean containsKey(Object key) {
        readLock.lock();
        boolean containsKey = entries.containsKey(key);
        readLock.unlock();
        return containsKey;

    }

    @Override
    public V get(Object key) {
        readLock.lock();
        V value = entries.get(key);
        readLock.unlock();
        return value;
    }

    @Override
    public V remove(Object key) {
        writeLock.lock();
        entries.remove(key);
        writeLock.unlock();
        return null;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;

    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        return null;
    }

    @Override
    public Set<K> keySet() {
        readLock.lock();
        Set<K> set = entries.keySet();
        readLock.unlock();
        return set;
    }

    @Override
    public Collection<V> values() {

        readLock.lock();
        Collection<V> values = entries.values();
        readLock.unlock();
        return values;
    }

    @Override
    public boolean containsValue(Object key) {
        readLock.lock();
        entries.containsValue(key);
        readLock.unlock();
        return false;
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        readLock.lock();
        entries.putAll(m);
        readLock.unlock();

    }

    @Override
    public void clear() {
        readLock.lock();
        entries.clear();
        readLock.unlock();
    }

}
