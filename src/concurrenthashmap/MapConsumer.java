package concurrenthashmap;

import java.util.Map;

public class MapConsumer implements Runnable {
    private final Map<String, Integer> elements;
    private final int elementsToGet;
    private final int from;

    public MapConsumer(Map<String, Integer> elements, int from, int elementsToGet) {
        super();
        this.elements = elements;
        this.elementsToGet = elementsToGet;
        this.from = from;
    }

    @Override
    public void run() {
        for (int i = from; i < from + elementsToGet; i++) {
            elements.get("index" + i);
        }

    }
}
