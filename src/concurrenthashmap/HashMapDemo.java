package concurrenthashmap;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HashMapDemo {
    public static void main(String[] args) throws InterruptedException {
        showOneLockMap();
        showSegmentedMap();
        showSynchronizedMap();
        showConcurrentHashMap();
    }

    public static void showOneLockMap() throws InterruptedException {

        Map<String, Integer> mapNotSoConcurrent = new ConcurrentHashMapSingleLockImplementation<>();

        List<Thread> threads2 = new ArrayList<>();
        for (int i = 4000; i < 8000; i++) {
            mapNotSoConcurrent.put("index" + i, i);

        }
        int from = 0;
        long startingTime2 = System.currentTimeMillis();
        for (int i = 0; i < 4; i++) {
            Thread thread = new Thread(new MapProducer(mapNotSoConcurrent, from, 1000000));
            from += 1000;
            threads2.add(thread);
            thread.start();
        }
        from = 4000;
        for (int i = 0; i < 4; i++) {
            Thread thread = new Thread(new MapConsumer(mapNotSoConcurrent, from, 1000000));
            from += 1000;
            threads2.add(thread);
            thread.start();
        }
        for (Thread thread : threads2) {
            thread.join();
        }
        System.out.println("Time taken synchronized with single lock: " + (System.currentTimeMillis() - startingTime2));
    }

    public static void showSegmentedMap() throws InterruptedException {

        Map<String, Integer> mapNotSoConcurrent = new ConcurrentHashMapSegmentsImplementation<>();
        List<Thread> threads2 = new ArrayList<>();
        for (int i = 4000; i < 8000; i++) {
            mapNotSoConcurrent.put("index" + i, i);

        }
        int from = 0;
        long startingTime2 = System.currentTimeMillis();
        for (int i = 0; i < 4; i++) {
            Thread thread = new Thread(new MapProducer(mapNotSoConcurrent, from, 1000000));
            from += 1000;
            threads2.add(thread);
            thread.start();
        }
        from = 4000;
        for (int i = 0; i < 4; i++) {
            Thread thread = new Thread(new MapConsumer(mapNotSoConcurrent, from, 1000000));
            from += 1000;
            threads2.add(thread);
            thread.start();
        }
        for (Thread thread : threads2) {
            thread.join();
        }
        System.out.println("Time taken segments implementation: " + (System.currentTimeMillis() - startingTime2));
    }

    public static void showSynchronizedMap() throws InterruptedException {

        Map<String, Integer> mapNotSoConcurrent = Collections.synchronizedMap(new HashMap<String, Integer>());

        List<Thread> threads2 = new ArrayList<>();
        for (int i = 4000; i < 8000; i++) {
            mapNotSoConcurrent.put("index" + i, i);

        }
        int from = 0;
        long startingTime2 = System.currentTimeMillis();
        for (int i = 0; i < 4; i++) {
            Thread thread = new Thread(new MapProducer(mapNotSoConcurrent, from, 1000000));
            from += 1000;
            threads2.add(thread);
            thread.start();
        }
        from = 4000;
        for (int i = 0; i < 4; i++) {
            Thread thread = new Thread(new MapConsumer(mapNotSoConcurrent, from, 1000000));
            from += 1000;
            threads2.add(thread);
            thread.start();
        }
        for (Thread thread : threads2) {
            thread.join();
        }
        System.out.println("Time taken java synchronized map class: " + (System.currentTimeMillis() - startingTime2));
    }

    public static void showConcurrentHashMap() throws InterruptedException {

        Map<String, Integer> mapNotSoConcurrent = new java.util.concurrent.ConcurrentHashMap<>();

        List<Thread> threads2 = new ArrayList<>();
        for (int i = 4000; i < 8000; i++) {
            mapNotSoConcurrent.put("index" + i, i);

        }
        int from = 0;
        long startingTime2 = System.currentTimeMillis();
        for (int i = 0; i < 4; i++) {
            Thread thread = new Thread(new MapProducer(mapNotSoConcurrent, from, 1000000));
            from += 1000;
            threads2.add(thread);
            thread.start();
        }
        from = 4000;
        for (int i = 0; i < 4; i++) {
            Thread thread = new Thread(new MapConsumer(mapNotSoConcurrent, from, 1000000));
            from += 1000;
            threads2.add(thread);
            thread.start();
        }
        for (Thread thread : threads2) {
            thread.join();
        }
        System.out.println("Time taken concurrent hash map: " + (System.currentTimeMillis() - startingTime2));
    }

}
