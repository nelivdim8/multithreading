package parallelcopy;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class CopyFileExecutor {
    private FileCopy copy;

    public String getInput(String regex) throws InvalidInputException {
        Scanner input = new Scanner(System.in);
        String text = input.nextLine();
        if (text.matches(regex)) {
            return text;
        }
        throw new InvalidInputException(text + " is not valid!");
    }

    public boolean getInput() {

        System.out.println(
                "To make a copy enter:\njava cp ~/Desktop/source/ /Desktop/target/ or java cp ~/Desktop/source/ /Desktop/target/ -s");
        boolean result;
        try {
            long startingTime = System.currentTimeMillis();
            this.copy = validateInput();
            result = this.copy.copy();
            System.out.println(System.currentTimeMillis() - startingTime);
        } catch (InvalidInputException e1) {
            System.out.println(e1.getMessage());
            return false;
        } catch (IOException e) {
            System.out.println("File cannot be copied");
            return false;
        }
        return result;

    }

    private FileCopy validateInput() throws InvalidInputException {

        String text = getInput("java\\scp\\s[[A-Z]:/A-Za-z0-9]+/\\s[[A-Z]:/A-Za-z0-9]+/(\\s-s)?");
        String[] paths = getPaths(text);
        Path source = Paths.get(paths[0]);
        Path destination = Paths.get(paths[1]);
        if (text.endsWith("-s")) {
            return new SequentialCopy(source, destination);

        } else {
            return new ParallelCopy(source, destination);
        }

    }

    private String[] getPaths(String text) {
        int first = text.indexOf("cp") + 3;
        int last = text.lastIndexOf("/");
        String srcAndDest = text.substring(first, last);
        String paths = srcAndDest.replace("/ ", " ");
        String result = paths.replace("\\", "\\\\");
        return result.split(" ");

    }

}