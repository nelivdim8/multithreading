package parallelcopy;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class SequentialCopy extends FileCopy {

    public SequentialCopy(Path source, Path destination) {
        super(source, destination);

    }

    private void copyAllFilesSequentially(List<Path> allFiles) throws IOException {
        for (int i = 0; i < allFiles.size(); i++) {
            copyFileSequentially(allFiles.get(i));
        }
    }

    private void copyFileSequentially(Path path) throws IOException {
        Path newDest = super.getDestination()
                .resolve(path.subpath(super.getSource().getNameCount() - 1, path.getNameCount() - 1));
        if (!Files.isDirectory(newDest)) {
            Files.createDirectories(newDest);

        }
        Files.copy(path, newDest.resolve(path.getFileName()));
    }

    @Override
    public boolean copy() throws IOException {
        copyAllFilesSequentially(super.getAllFiles());

        return false;
    }
}
