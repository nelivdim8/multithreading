package parallelcopy;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SeekableByteChannel;
import java.util.concurrent.RecursiveAction;

public class LargeFilesWriting extends RecursiveAction {
    private final static long THRESHOLD_SIZE = 100_000;
    private int from;
    private int to;
    private SeekableByteChannel channelSrc;
    private SeekableByteChannel channelDest;

    public LargeFilesWriting(int from, int to, SeekableByteChannel channelSrc, SeekableByteChannel channelDest) {
        super();
        this.channelSrc = channelSrc;
        this.channelDest = channelDest;
        this.from = from;
        this.to = to;

    }

    @Override
    protected void compute() {
        int length = to - from;
        if (length < THRESHOLD_SIZE) {
            try {
                ByteBuffer in = ByteBuffer.allocate(length);
                channelSrc.position(from);
                channelSrc.read(in);
                in.rewind();
                channelDest.position(from);
                channelDest.write(in);
                in.flip();

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else {
            int delta = length / 2;
            LargeFilesWriting task1 = new LargeFilesWriting(from, from + delta, channelSrc, channelDest);
            task1.fork();
            LargeFilesWriting task2 = new LargeFilesWriting(from + delta, to, channelSrc, channelDest);
            task2.fork();
            task1.join();
            task2.join();

        }
    }

}
