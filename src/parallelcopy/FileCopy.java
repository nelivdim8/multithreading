package parallelcopy;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

public abstract class FileCopy {
    private Path source;
    private Path destination;

    public FileCopy(Path source, Path destination) {
        super();
        this.source = source;
        this.destination = destination;
    }

    public Path getSource() {
        return source;
    }

    public Path getDestination() {
        return destination;
    }

    public List<Path> getAllFiles() {
        final List<Path> allFiles = new ArrayList<>();
        try {
            Files.walkFileTree(source, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    if (!attrs.isDirectory()) {
                        allFiles.add(file);
                    }
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        return allFiles;

    }

    public abstract boolean copy() throws IOException;
}
