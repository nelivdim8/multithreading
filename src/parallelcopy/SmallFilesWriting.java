package parallelcopy;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.concurrent.RecursiveAction;

public class SmallFilesWriting extends RecursiveAction {
    private final static long THRESHOLD_SIZE = 100_000;
    private List<Path> files;
    private Path source;
    private Path destination;
    private int from;
    private int to;

    public SmallFilesWriting(List<Path> files, Path source, Path destination, int from, int to) {
        super();
        this.files = files;
        this.destination = destination;
        this.from = from;
        this.to = to;
        this.source = source;

    }

    @Override
    protected void compute() {
        int length = to - from;
        if (getFilesSize(from, to) < THRESHOLD_SIZE) {
            try {
                copyFiles(from, to);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else {
            int delta = length / 2;
            SmallFilesWriting task1 = new SmallFilesWriting(files, source, destination, from, from + delta);
            task1.fork();
            SmallFilesWriting task2 = new SmallFilesWriting(files, source, destination, from + delta, to);
            task2.fork();

        }

    }

    private long getFilesSize(int from, int to) {
        long sumOfSizes = 0;
        for (int i = from; i < to; i++) {
            sumOfSizes += files.get(i).toFile().length();
        }
        return sumOfSizes;

    }

    private void copyFiles(int from, int to) throws IOException {
        for (int i = from; i < to; i++) {
            copyFile(files.get(i));

        }
    }

    private void copyFile(Path path) throws IOException {
        Path newDest = destination.resolve(path.subpath(source.getNameCount() - 1, path.getNameCount() - 1));
        if (!Files.isDirectory(newDest)) {
            Files.createDirectories(newDest);
        }

        Files.createFile(newDest.resolve(path.getFileName()));

    }
}
