package parallelcopy;

import java.io.IOException;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ForkJoinPool;

public class ParallelCopy extends FileCopy {

    public ParallelCopy(Path source, Path destination) {
        super(source, destination);

    }

    private void copyAllFilesParallelly(List<Path> allFiles) throws IOException {
        ForkJoinPool forkJoinPool = new ForkJoinPool(Runtime.getRuntime().availableProcessors());
        List<Path> smallFiles = new ArrayList<>();
        int threshold = 100_000;
        for (int i = 0; i < allFiles.size(); i++) {
            if (allFiles.get(i).toFile().length() >= threshold) {
                Path newDest = super.getDestination().resolve(
                        allFiles.get(i).subpath(super.getSource().getNameCount() - 1, allFiles.get(i).getNameCount()));
                copyLargeFile(forkJoinPool, allFiles.get(i), newDest);
            } else {

                smallFiles.add(allFiles.get(i));
            }
        }
        copySmallFiles(forkJoinPool, smallFiles);
    }

    private void copyLargeFile(ForkJoinPool forkJoinPool, Path src, Path destination) throws IOException {
        if (!Files.isDirectory(destination.getParent())) {
            Files.createDirectories(destination.getParent());
        }
        Files.createFile(destination);
        SeekableByteChannel channelSrc = Files.newByteChannel(src, StandardOpenOption.READ);
        SeekableByteChannel channelDest = Files.newByteChannel(destination, StandardOpenOption.WRITE);
        int length = (int) src.toFile().length();
        LargeFilesWriting copy = new LargeFilesWriting(0, length, channelSrc, channelDest);
        forkJoinPool.invoke(copy);

    }

    private void copySmallFiles(ForkJoinPool forkJoinPool, List<Path> smallFiles) throws IOException {
        SmallFilesWriting copy = new SmallFilesWriting(smallFiles, super.getSource(), super.getDestination(), 0,
                smallFiles.size());
        forkJoinPool.invoke(copy);

    }

    @Override
    public boolean copy() throws IOException {
        copyAllFilesParallelly(super.getAllFiles());
        return false;
    }

}
