package matrixmultiplication;

import java.util.Random;

public class RandomGenerator {
    private final static Random random = new Random();

    public static int[][] generateNumbers(int rows, int columns, int from, int length) {
        int[][] array = new int[rows][columns];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                array[i][j] = from + random.nextInt(length);

            }
        }
        return array;
    }
}
