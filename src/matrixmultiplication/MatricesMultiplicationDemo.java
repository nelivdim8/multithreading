package matrixmultiplication;

public class MatricesMultiplicationDemo {
    public static void main(String[] args) throws InterruptedException {
        int[][] matrix1 = RandomGenerator.generateNumbers(801, 1003, 0, 100);
        int[][] matrix2 = RandomGenerator.generateNumbers(1003, 867, 0, 100);
        Matrix first = new Matrix(matrix1);
        Matrix second = new Matrix(matrix2);
        long timeParallel = System.currentTimeMillis();
        Matrix parallelResult = MultiplicationOfMatrices.parallelMultiply(first, second);
        System.out.println("parallel " + (System.currentTimeMillis() - timeParallel));
        long timeSequential = System.currentTimeMillis();
        Matrix sequentialResult = MultiplicationOfMatrices.sequentialMultiply(first, second);
        System.out.println("sequential " + (System.currentTimeMillis() - timeSequential));
        System.out.println(sequentialResult.equals(parallelResult));

    }
}
