package matrixmultiplication;

import java.util.Arrays;

public class Matrix {
    private final int[][] matrix;
    private final int rows;
    private final int columns;

    public Matrix(int[][] matrix) {
        super();
        this.matrix = matrix;
        this.rows = matrix.length;
        this.columns = matrix[0].length;

    }

    public Matrix(int rows, int columns) {
        super();
        this.rows = rows;
        this.columns = columns;
        this.matrix = new int[rows][columns];

    }

    public int[][] getMatrix() {
        return matrix;
    }

    public int getRows() {
        return rows;
    }

    public int getCell(int i, int j) {
        return matrix[i][j];

    }

    public void setCell(int i, int j, int value) {
        this.matrix[i][j] = value;

    }

    public int getColumns() {
        return columns;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + columns;
        result = prime * result + Arrays.deepHashCode(matrix);
        result = prime * result + rows;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Matrix other = (Matrix) obj;
        if (columns != other.columns)
            return false;

        if (rows != other.rows)
            return false;
        return Arrays.deepEquals(matrix, other.matrix);
    }

    public void print() {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                System.out.print(matrix[i][j] + "  ");
            }
            System.out.println();
        }
        System.out.println();
    }

}
