package matrixmultiplication;

public class ParallelMultiplier implements Runnable {
    private final Matrix first;
    private final Matrix second;
    private final Matrix result;
    private final int from;
    private final int delta;

    public ParallelMultiplier(Matrix first, Matrix second, Matrix result, int from, int delta) {
        super();
        this.first = first;
        this.second = second;
        this.result = result;
        this.from = from;
        this.delta = delta;
    }

    @Override
    public void run() {
        for (int i = from; i < from + delta; i++) {
            for (int j = 0; j < second.getColumns(); j++) {
                int value = 0;
                for (int k = 0; k < first.getColumns(); k++) {
                    value += first.getCell(i, k) * second.getCell(k, j);
                }
                result.setCell(i, j, value);

            }
        }

    }

}
