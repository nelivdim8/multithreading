package matrixmultiplication;

import java.util.ArrayList;
import java.util.List;

public class MultiplicationOfMatrices {

    public static Matrix sequentialMultiply(Matrix first, Matrix second) {
        if (!isValidMultiplication(first, second)) {
            throw new IllegalArgumentException();
        }
        Matrix result = new Matrix(first.getRows(), second.getColumns());
        for (int i = 0; i < first.getRows(); i++) {
            for (int j = 0; j < second.getColumns(); j++) {
                int value = 0;
                for (int j2 = 0; j2 < first.getColumns(); j2++) {
                    value += first.getCell(i, j2) * second.getCell(j2, j);
                }
                result.setCell(i, j, value);
            }

        }
        return result;
    }

    public static Matrix parallelMultiply(Matrix first, Matrix second) throws InterruptedException {
        if (!isValidMultiplication(first, second)) {
            throw new IllegalArgumentException();
        }
        Matrix result = new Matrix(first.getRows(), second.getColumns());
        int numberOfThreads = Runtime.getRuntime().availableProcessors();
        int row = 0;
        int delta = first.getRows() / numberOfThreads;
        List<Thread> threads = new ArrayList<>();
        for (int i = 0; i < numberOfThreads; i++) {
            Thread t = new Thread(new ParallelMultiplier(first, second, result, row, delta));
            t.start();
            threads.add(t);
            row += delta;

        }
        Thread t = new Thread(new ParallelMultiplier(first, second, result, row, first.getRows() % numberOfThreads));
        t.start();
        threads.add(t);
        for (Thread thread : threads) {
            thread.join();
        }

        return result;
    }

    private static boolean isValidMultiplication(Matrix first, Matrix second) {
        return first != null && second != null && first.getColumns() == second.getRows();
    }

}
