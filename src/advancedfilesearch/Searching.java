package advancedfilesearch;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Searching {
    public static void searchFileForText(File file, String text, Result result) throws FileNotFoundException {
        int linesCount = 0;
        try (Scanner input = new Scanner(file)) {
            while (input.hasNext()) {
                String line = input.nextLine();
                if (line.indexOf(text) > -1) {
                    result.addMatching(file, linesCount);
                    return;
                }
                linesCount++;
            }
        }

    }

    public static void parallelSarching(Folder folder, String text) throws InterruptedException {
        Result result = new Result();
        List<File> files = folder.getAllFiles();
        int threadsCount = Runtime.getRuntime().availableProcessors();
        ExecutorService executor = Executors.newFixedThreadPool(threadsCount);
        if (threadsCount > files.size()) {
            threadsCount = files.size();
        }
        int from = 0;
        int length = 1;
        for (int i = 0; i < threadsCount; i++) {
            executor.execute(new FileContentSearching(files, from, length, text, result));
            from += length;

        }
        executor.shutdown();
        executor.awaitTermination(10, TimeUnit.SECONDS);
        result.print();
    }

    public static void sequentialSearching(Folder folder, String text) throws IOException {
        Result result = new Result();
        List<File> files = folder.getAllFiles();
        sequentialSearchAllFiles(files, text, result);
        result.print();
    }

    private static void sequentialSearchAllFiles(List<File> files, String text, Result result) throws IOException {
        for (int i = 0; i < files.size(); i++) {
            searchFileForText(files.get(i), text, result);
        }
    }

}
