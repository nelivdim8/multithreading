package advancedfilesearch;

import java.io.IOException;
import java.nio.file.Paths;

public class AdvancedFileSearchDemo {
    public static void main(String[] args) throws IOException, InterruptedException {
        Folder folder = new Folder(Paths.get(System.getProperty("user.dir")).resolve("folder"));
        long startingTime = System.currentTimeMillis();
        Searching.parallelSarching(folder, "stores the elements internally");
        System.out.println("Parallel time taken " + (System.currentTimeMillis() - startingTime));

        long startingTime2 = System.currentTimeMillis();
        Searching.sequentialSearching(folder, "stores the elements internally");
        System.out.println("Sequential time taken " + (System.currentTimeMillis() - startingTime2));

    }
}
