package advancedfilesearch;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class Result {
    private final Map<File, Integer> result;
    private final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();

    public Result(Map<File, Integer> result) {
        super();
        this.result = result;
    }

    public Result() {
        this.result = new HashMap<>();
    }

    public void addMatching(File file, Integer line) {
        lock.writeLock().lock();
        result.put(file, line);
        lock.writeLock().unlock();
    }

    public void print() {
        lock.readLock().lock();
        result.entrySet().forEach(x -> System.out.println("File name: " + x.getKey() + " line: " + x.getValue()));
        lock.readLock().unlock();
    }

}
