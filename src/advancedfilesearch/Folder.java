package advancedfilesearch;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

public class Folder {
    private final Path source;

    public Folder(Path source) {
        super();
        this.source = source;
    }

    public List<File> getAllFiles() {
        final List<File> files = new ArrayList<>();
        try {
            Files.walkFileTree(source, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    if (!attrs.isDirectory() && isATxtFile(file.toFile())) {
                        files.add(file.toFile());
                    }
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        return files;

    }

    public boolean isATxtFile(File file) throws IOException {
        return file.getName().endsWith(".txt");

    }

}
