package advancedfilesearch;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class FileContentSearching implements Runnable {
    private final List<File> files;
    private final int from;
    private final int length;
    private final String text;
    private final Result result;

    public FileContentSearching(List<File> files, int from, int length, String text, Result result) {
        super();
        this.files = files;
        this.from = from;
        this.length = length;
        this.text = text;
        this.result = result;

    }

    @Override
    public void run() {
        for (int i = from; i < from + length; i++) {
            try {
                Searching.searchFileForText(files.get(i), text, result);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

}
