package imageresizer;

import java.util.concurrent.CountDownLatch;

public class Start {
    public static void main(String[] args) throws InterruptedException {
        ImageResizer resizer = null;
        try {
            resizer = InputReader.getResizer();
        } catch (InvalidInputException e) {
            System.out.println(e.getMessage());
        }
        CountDownLatch latch = new CountDownLatch(1);
        long startingTime = System.currentTimeMillis();
        Thread worker2 = new Thread(new Renamer(resizer, latch));
        worker2.setDaemon(true);
        worker2.start();
        Thread worker1 = new Thread(new Resizer(resizer, latch));
        worker1.setDaemon(true);
        worker1.start();

        System.out.println(System.currentTimeMillis() - startingTime);
        for (int i = 0; i < 1000; i++) {
            System.out.println("Doing some work...");
            Thread.sleep(1000);
        }

    }

}
