package imageresizer;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;

public class Renamer implements Runnable {
    private ImageResizer imgResizer;
    private final CountDownLatch latch;

    public Renamer(ImageResizer imgResizer, CountDownLatch latch) {
        super();
        this.imgResizer = imgResizer;
        this.latch = latch;
    }

    @Override
    public void run() {
        try {
            imgResizer.createDestinations();
            latch.countDown();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

}
