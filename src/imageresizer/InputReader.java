package imageresizer;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class InputReader {

    public static String validateInput(String regex) throws InvalidInputException {
        Scanner input = new Scanner(System.in);
        String text = input.nextLine();
        if (text.matches(regex)) {
            return text;
        }
        throw new InvalidInputException(text + " is not valid!");
    }

    public static ImageResizer getResizer() throws InvalidInputException {
        showMessage();
        String text = validateInput("java\\sResizeImage(\\s-r)?\\s[[A-Z]:/A-Za-z0-9]+/\\s[0-9]+\\s[0-9]+");
        String[] paths = splitInput(text);
        Path source = Paths.get(paths[0]);
        double width = Integer.parseInt(paths[1]);
        double height = Integer.parseInt(paths[2]);
        return new ImageResizer(source, width, height, isRecursive(text));

    }

    private static boolean isRecursive(String text) {
        return text.contains(" -r ");
    }

    private static String getPathAndSize(String text) {
        if (text.contains(" -r ")) {
            return text.replace(" -r ", " ");

        }
        return text;
    }

    private static String[] splitInput(String text) {
        String srcWidthAndHeight = getPathAndSize(text);
        int first = text.indexOf(":");
        String srcAndDest = text.substring(first - 1);
        String paths = srcAndDest.replace("/ ", " ");
        String result = paths.replace("\\", "\\\\");

        return result.split(" ");

    }

    private static void showMessage() {
        System.out.println(
                "To make a copy enter:\njava ResizeImage ~/Desktop/source/ new width new height or java ResizeImage -r ~/Desktop/source/ new width new height");
    }

}
