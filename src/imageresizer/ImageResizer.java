package imageresizer;

import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import javax.imageio.ImageIO;

public class ImageResizer {
    private final Path srcFolder;
    private final static String RESIZED_IMAGES_FOLDER_NAME = "resized_pictures";
    private final double widthCoeff;;
    private final double heightCoeff;
    private final boolean isRecursive;
    private final List<Path> imagesToResize;

    public ImageResizer(Path source, double widthCoeff, double heightCoeff, boolean isResursive) {
        super();
        this.srcFolder = source;
        this.widthCoeff = widthCoeff;
        this.heightCoeff = heightCoeff;
        this.isRecursive = isResursive;
        this.imagesToResize = getAllFiles((isRecursive) ? Integer.MAX_VALUE : 1);

    }

    public int getImagesCount() {
        return imagesToResize.size();
    }

    public List<Path> getAllFiles(int depth) {
        final List<Path> allFiles = new ArrayList<>();
        try {
            Files.walkFileTree(srcFolder, EnumSet.of(FileVisitOption.FOLLOW_LINKS), depth,
                    new SimpleFileVisitor<Path>() {
                        @Override
                        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                            if (!attrs.isDirectory() && isAnImage(file)) {
                                allFiles.add(file);
                            }
                            return FileVisitResult.CONTINUE;
                        }

                        private boolean isAnImage(Path path) throws IOException {
                            String mimetype = Files.probeContentType(path);
                            return mimetype != null && mimetype.split("/")[0].equals("image");

                        }
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }
        return allFiles;

    }

    public void resizeAllFiles() throws IOException {
        for (int i = 0; i < imagesToResize.size(); i++) {
            System.out.println(Thread.currentThread().getName() + " is resizer");

            resizeImage(imagesToResize.get(i), imagesToResize.get(i).getParent().resolve(RESIZED_IMAGES_FOLDER_NAME)
                    .resolve(imagesToResize.get(i).getFileName()));

        }

    }

    public void createDestinations() throws IOException {
        for (Path path : imagesToResize) {
            System.out.println(Thread.currentThread().getName() + " is renamer");
            createDestination(path);
        }
    }

    private Path createDestination(Path path) throws IOException {
        Path newDest = path.getParent().resolve(RESIZED_IMAGES_FOLDER_NAME);
        System.out.println(newDest.toString());
        if (!Files.isDirectory(newDest)) {
            Files.createDirectories(newDest);

        }
        return newDest;

    }

    private static int[][] resize(int[][] pixels, int newWidth, int newHeight) {
        int[][] resizedArray = new int[newHeight][newWidth];
        int width = pixels[0].length;
        int height = pixels.length;
        double ratioRow = (double) width / newWidth;
        double ratioColumn = (double) height / newHeight;
        double x;
        double y;
        for (int i = 0; i < newHeight; i++) {
            for (int j = 0; j < newWidth; j++) {
                x = Math.floor(j * ratioRow);
                y = Math.floor(i * ratioColumn);
                resizedArray[i][j] = pixels[(int) y][(int) x];
            }
        }

        return resizedArray;
    }

    public void resizeImage(Path source, Path destination) {
        int[][] pixels = readImage(source.toFile());
        int newWidth = (int) (pixels[0].length * widthCoeff);
        int newHeight = (int) (pixels.length * heightCoeff);
        int[][] resizedImage = resize(pixels, newWidth, newHeight);
        writeResizedImage(destination.toFile(), resizedImage);
    }

    private int[][] readImage(File filename) {
        BufferedImage img = null;
        try {
            img = ImageIO.read(filename);
            int width = img.getWidth();
            int height = img.getHeight();
            int[][] pixels = new int[height][width];

            for (int i = 0; i < height; i++) {
                for (int j = 0; j < width; j++) {
                    int pixel = img.getRGB(j, i);
                    pixels[i][j] = pixel;
                }
            }

            return pixels;
        } catch (IOException e) {
            e.printStackTrace();
            return new int[0][0];
        }
    }

    private File writeResizedImage(File dest, int[][] pixels) {
        BufferedImage img = new BufferedImage(pixels[0].length, pixels.length, BufferedImage.TYPE_INT_RGB);
        WritableRaster raster = img.getRaster();
        int[] ar = new int[pixels[0].length * pixels.length];
        for (int i = 0; i < pixels.length; i++) {
            for (int j = 0; j < pixels[i].length; j++) {
                ar[pixels[0].length * i + j] = pixels[i][j];
            }

        }
        raster.setDataElements(0, 0, pixels[0].length, pixels.length, ar);

        try {
            ImageIO.write(img, "jpg", dest);
        } catch (IOException x) {
            x.printStackTrace();
        }
        return dest;

    }
}
