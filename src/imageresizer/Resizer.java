package imageresizer;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;

public class Resizer implements Runnable {
    private ImageResizer imgResizer;
    private final CountDownLatch latch;

    public Resizer(ImageResizer imgResizer, CountDownLatch latch) {
        super();
        this.imgResizer = imgResizer;
        this.latch = latch;
    }

    @Override
    public void run() {
        try {
            latch.await();
            imgResizer.resizeAllFiles();

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
