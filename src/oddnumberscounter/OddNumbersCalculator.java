package oddnumberscounter;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class OddNumbersCalculator {
    private int[] numbers;

    public OddNumbersCalculator(int[] numbers) {
        super();
        this.numbers = numbers;
    }

    public int calculateOddNumbersSequentially() {
        int sum = 0;
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] % 2 != 0) {
                sum++;
            }
        }
        return sum;
    }

    public int calculateOddNumbersParallely() throws InterruptedException, ExecutionException {
        int numberOfThreads = Runtime.getRuntime().availableProcessors();
        ExecutorService executor = Executors.newFixedThreadPool(numberOfThreads);
        List<Future<Integer>> futures = new ArrayList<>();
        int from = 0;
        int delta = numbers.length / numberOfThreads;
        for (int i = 0; i < numberOfThreads; i++) {

            futures.add(executor.submit(new Counter(numbers, from, delta)));
            from += delta;

        }
        futures.add(executor.submit(new Counter(numbers, from, numbers.length % numberOfThreads)));
        int sum = 0;
        for (Future<Integer> future : futures) {
            sum += future.get();
        }

        return sum;

    }
}
