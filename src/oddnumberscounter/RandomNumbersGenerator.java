package oddnumberscounter;

import java.util.Random;

public class RandomNumbersGenerator {
    private final static Random random = new Random();

    public static int[] generateNumbers(int count, int from, int length) {
        int[] numbers = new int[count];
        for (int i = 0; i < count; i++) {
            numbers[i] = from + random.nextInt(length);
        }
        return numbers;
    }
}
