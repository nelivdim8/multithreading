package oddnumberscounter;

import java.util.concurrent.Callable;

public class Counter implements Callable<Integer> {
    private int[] numbers;
    private int from;
    private int length;

    public Counter(int[] numbers, int from, int length) {
        super();
        this.numbers = numbers;
        this.from = from;
        this.length = length;
    }

    @Override
    public Integer call() throws Exception {
        int counter = 0;
        for (int i = from; i < from + length; i++) {
            if (numbers[i] % 2 != 0) {
                counter++;
            }

        }
        return counter;
    }

}
