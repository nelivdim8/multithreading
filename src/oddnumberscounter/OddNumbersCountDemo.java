package oddnumberscounter;

import java.util.concurrent.ExecutionException;

public class OddNumbersCountDemo {
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        OddNumbersCalculator numbers = new OddNumbersCalculator(
                RandomNumbersGenerator.generateNumbers(2_000_000_000, 0, Integer.MAX_VALUE));
        long startingTime = System.currentTimeMillis();
        System.out.println(numbers.calculateOddNumbersSequentially());
        System.out.println("Sequential " + (System.currentTimeMillis() - startingTime));
        long startingTime2 = System.currentTimeMillis();
        System.out.println(numbers.calculateOddNumbersParallely());
        System.out.println("Parallel " + (System.currentTimeMillis() - startingTime2));

    }

}
