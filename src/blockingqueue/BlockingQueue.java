package blockingqueue;

import java.util.LinkedList;
import java.util.Queue;

public class BlockingQueue<T> {
    private Queue<T> elements;
    private final static int MAX_SIZE = 20;
    private Lock outerLock = new Lock();
    private Lock lockEmpty = new Lock();
    private Lock lockFull = new Lock();

    public BlockingQueue() {
        super();
        this.elements = new LinkedList<>();

    }

    public T poll() throws InterruptedException {
        outerLock.lock();
        while (elements.isEmpty()) {
            System.out.println("Queue is empty!");
            lockEmpty.lock();

        }
        T element = elements.poll();
        lockFull.unlock();
        outerLock.unlock();
        return element;
    }

    public boolean offer(T element) throws InterruptedException {
        outerLock.lock();
        while (elements.size() == MAX_SIZE) {
            System.out.println("Queue is full!");
            lockFull.lock();

        }
        boolean isAdded = elements.add(element);
        lockEmpty.unlock();
        outerLock.unlock();
        return isAdded;
    }

}
