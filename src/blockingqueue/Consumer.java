package blockingqueue;

public class Consumer<T> implements Runnable {
    private BlockingQueue<T> queue;
    private int elementsToRemove;

    public Consumer(BlockingQueue<T> queue, int elementsToRemove) {
        super();
        this.queue = queue;
        this.elementsToRemove = elementsToRemove;
    }

    @Override
    public void run() {
        for (int i = 0; i < elementsToRemove; i++) {

            try {
                queue.poll();
                System.out.println("Thread " + Thread.currentThread().getName() + "- remove " + i);

            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }
    }
}
