package blockingqueue;

public class BlockingQueueDemo {
    public static void main(String[] args) {
        BlockingQueue<Integer> queue = new BlockingQueue<>();
        for (int i = 0; i < 2; i++) {
            Runnable task = new Producer(queue, 15);
            Thread worker = new Thread(task);
            worker.setName(String.valueOf(i));
            worker.start();

        }
        for (int i = 0; i < 2; i++) {
            Runnable task = new Consumer<>(queue, 10);
            Thread worker = new Thread(task);
            worker.setName(String.valueOf(i));
            worker.start();

        }

    }
}
