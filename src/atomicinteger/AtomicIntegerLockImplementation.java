package atomicinteger;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.IntBinaryOperator;
import java.util.function.IntUnaryOperator;

public class AtomicIntegerLockImplementation {
    private int value;
    private ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
    private Lock readLock = lock.readLock();
    private Lock writeLock = lock.writeLock();

    public AtomicIntegerLockImplementation(int value) {
        super();
        this.value = value;
    }

    public AtomicIntegerLockImplementation() {
        super();
        this.value = 0;

    }

    public int get() {

        readLock.lock();
        int value = this.value;
        readLock.unlock();
        return value;

    }

    public void set(int newValue) {
        writeLock.lock();
        this.value = newValue;
        writeLock.unlock();

    }

    public int getAndIncrement() {

        writeLock.lock();
        int value = this.value;
        this.value++;
        writeLock.unlock();
        return value;

    }

    public int incrementAndGet() {
        writeLock.lock();
        this.value++;
        writeLock.unlock();
        return this.value;
    }

    public int getAndDecrement() {
        writeLock.lock();
        int value = this.value;
        this.value--;
        writeLock.unlock();
        return value;

    }

    public int decrementAndGet() {
        writeLock.lock();
        this.value--;
        writeLock.unlock();
        return this.value;

    }

    public int getAndSet(int newValue) {
        writeLock.lock();
        int value = this.value;
        this.value = newValue;
        writeLock.unlock();
        return value;
    }

    public int accumulateAndGet(int x, IntBinaryOperator accumulatorFunction) {
        writeLock.lock();
        this.value = accumulatorFunction.applyAsInt(this.value, x);
        writeLock.unlock();
        return this.value;
    }

    public int getAndAccumulate(int x, IntBinaryOperator accumulatorFunction) {

        writeLock.lock();
        int value = this.value;
        this.value = accumulatorFunction.applyAsInt(this.value, x);
        writeLock.unlock();
        return value;
    }

    public int updateAndGet(IntUnaryOperator updateFunction) {
        writeLock.lock();
        this.value = updateFunction.applyAsInt(this.value);
        writeLock.unlock();
        return this.value;
    }

    public int getAndUpdate(IntUnaryOperator updateFunction) {
        writeLock.lock();
        int value = this.value;
        this.value = updateFunction.applyAsInt(this.value);
        writeLock.unlock();
        return value;
    }

    public int getAndAdd(int delta) {

        writeLock.lock();
        int value = this.value;
        this.value += delta;
        writeLock.unlock();
        return value;
    }

    public int addAndGet(int delta) {
        writeLock.lock();
        this.value += delta;
        writeLock.unlock();
        return this.value;

    }

    public boolean compareAndSet(int expect, int update) {
        boolean isUpdated = false;
        writeLock.lock();
        if (this.value == expect) {
            this.value = update;
            isUpdated = true;
        }
        writeLock.unlock();
        return isUpdated;

    }

}
