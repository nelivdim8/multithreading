package atomicinteger;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class AtomicIntegerDemo {
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        AtomicIntegerLockImplementation value = new AtomicIntegerLockImplementation();
        ExecutorService executor = Executors.newFixedThreadPool(4);
        List<Future> futures = new ArrayList<>();
        long startingTime = System.currentTimeMillis();
        for (int i = 1; i <= 4; i++) {
            Future future = executor.submit(() -> value.addAndGet(10));
            futures.add(future);

        }
        for (Future future : futures) {
            System.out.println(future.get());
        }
        executor.awaitTermination(5, TimeUnit.MILLISECONDS);
        System.out.println("Time1 " + (System.currentTimeMillis() - startingTime));

        AtomicInteger valueAtomic = new AtomicInteger();
        ExecutorService executorAtomic = Executors.newFixedThreadPool(4);
        List<Future> futuresAtomic = new ArrayList<>();
        long startingTimeAtomic = System.currentTimeMillis();
        for (int i = 1; i <= 4; i++) {
            Future future = executor.submit(() -> valueAtomic.addAndGet(10));
            futures.add(future);

        }
        for (Future future : futures) {
            System.out.println(future.get());
        }
        executor.awaitTermination(5, TimeUnit.MILLISECONDS);
        System.out.println("Time2" + (System.currentTimeMillis() - startingTimeAtomic));
    }
}
