package pointsmindistance;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class PointsGenerator {

    public static List<Point> generatePoints(int count, int from, int to) {
        Random random = new Random();
        List<Point> points = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            points.add(new Point(from + random.nextInt(to), from + random.nextInt(to)));
        }
        return points;
    }
}
