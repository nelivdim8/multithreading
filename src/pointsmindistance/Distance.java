package pointsmindistance;

public class Distance {
    private Point point;
    private double distance;

    public Distance(Point point, double distance) {
        super();
        this.point = point;
        this.distance = distance;
    }

    public Point getPoint() {
        return point;
    }

    public double getDistance() {
        return distance;
    }

    @Override
    public String toString() {
        return String.format("minimal distance is to - %s : %3.2f", point, distance);
    }

}
