package pointsmindistance;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class Plane {
    private List<Point> points;
    private Map<Point, Distance> distances = new HashMap<>();
    private ReadWriteLock lock = new ReentrantReadWriteLock();
    private Lock writeLock = lock.writeLock();

    public Plane(List<Point> points) {
        super();
        this.points = Collections.unmodifiableList(points);

    }

    private List<Distance> getDistances(Point point) {
        List<Distance> distances = new ArrayList<>();
        for (int i = 0; i < points.size(); i++) {
            if (!point.equals(points.get(i))) {

                distances.add(point.getDistanceTo(points.get(i)));

            }

        }
        return distances;
    }

    public void calculateDistance(int index) {
        List<Distance> distances = getDistances(points.get(index));
        Distance min = Collections.min(distances, Comparator.comparing(Distance::getDistance));
        writeLock.lock();
        System.out.println(points.get(index) + " - " + min);
        this.distances.put(points.get(index), min);
        writeLock.unlock();

    }

    public void getNearestPoints(int from, int to) {
        for (int i = from; i < to; i++) {
            calculateDistance(i);
        }
    }

    public void printMinimalDistancesFromEachPoint() throws InterruptedException {
        ForkJoinPool forkJoinPool = new ForkJoinPool(Runtime.getRuntime().availableProcessors());
        MinDistanceCalculator calculator = new MinDistanceCalculator(this, 0, points.size());
        forkJoinPool.invoke(calculator);

    }
}
