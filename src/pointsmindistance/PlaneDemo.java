package pointsmindistance;

public class PlaneDemo {
    public static void main(String[] args) throws InterruptedException {
        Plane plane = new Plane(PointsGenerator.generatePoints(100000, 0, 10000));
        long start = System.currentTimeMillis();
        plane.printMinimalDistancesFromEachPoint();
        System.out.println("Time " + (System.currentTimeMillis() - start));

    }
}
