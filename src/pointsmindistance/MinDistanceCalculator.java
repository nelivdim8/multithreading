package pointsmindistance;

import java.util.concurrent.RecursiveAction;

public class MinDistanceCalculator extends RecursiveAction {
    private Plane plane;
    private final static int THRESHOLD = 1000;
    private int from;
    private int to;

    public MinDistanceCalculator(Plane plane, int from, int to) {
        super();
        this.plane = plane;
        this.from = from;
        this.to = to;

    }

    @Override
    protected void compute() {
        int length = to - from;

        if (length < THRESHOLD) {
            plane.getNearestPoints(from, to);
        } else {
            int delta = length / 4;
            MinDistanceCalculator task1 = new MinDistanceCalculator(plane, from, from + delta);
            task1.fork();
            MinDistanceCalculator task2 = new MinDistanceCalculator(plane, from + delta, length);
            task2.fork();
            task1.join();
            task2.join();

        }

    }

}
